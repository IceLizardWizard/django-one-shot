from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList
from todos.form import TodoForm
# Create your views here.

def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list_key": todos
    }
    return render(request, "todos/list.html" , context)

def todo_list_detail(request, id):
    task = TodoList.objects.get(id=id)
    #this function references TodoList
    #then it creates context that details html references
    context = {
        "todo_tasks_context": task
    }
    return render(request, "todos/details.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", id=task.id)
    else:
        form = TodoForm()
    context = {
        "form_key" : form,
    }
    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    task = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=task)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", task.id)

    else:
        form = TodoForm(instance=task)
        context = {
        "form_up_key" : form
        }
    return render(request, "todos/edit.html", context)